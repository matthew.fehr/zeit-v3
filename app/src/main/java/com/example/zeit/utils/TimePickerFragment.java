package com.example.zeit.utils;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.zeit.PantallaAddTarea;
import com.example.zeit.Sistema;

import java.util.Calendar;

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        int hora;
        int minuto;

        if (PantallaAddTarea.horaInicio == -1 || PantallaAddTarea.horaFin == -1){
            Calendar c = Calendar.getInstance();
            hora = c.get(Calendar.HOUR_OF_DAY);
            minuto = c.get(Calendar.MINUTE);
        } else {
            if (PantallaAddTarea.esElInicioHora){
                hora = PantallaAddTarea.horaInicio;
                minuto = PantallaAddTarea.minutoInicio;
            } else {
                hora = PantallaAddTarea.horaFin;
                minuto = PantallaAddTarea.minutoFin;
            }
        }

        return new TimePickerDialog(getActivity(), (TimePickerDialog.OnTimeSetListener) this, hora, minuto, Sistema.formato24Horas);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        PantallaAddTarea.setTime(hourOfDay, minute);
    }
}
