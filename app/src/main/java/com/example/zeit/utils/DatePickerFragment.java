package com.example.zeit.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.zeit.PantallaAddTarea;
import com.example.zeit.Sistema;

import java.time.Year;
import java.util.Calendar;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        int year;
        int month;
        int day;

        if (PantallaAddTarea.yearInicio == -1 || PantallaAddTarea.yearFin == -1){
            Calendar c = Calendar.getInstance();
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        } else {
            if (PantallaAddTarea.esElInicioFecha){
                year = PantallaAddTarea.yearInicio;
                month = PantallaAddTarea.monthInicio;
                day = PantallaAddTarea.dayInicio;
            } else {
                year = PantallaAddTarea.yearFin;
                month = PantallaAddTarea.monthFin;
                day = PantallaAddTarea.dayFin;
            }
        }

        //android.app.Application ap = getActivity().getApplicationContext();
        return new DatePickerDialog(getActivity(), (DatePickerDialog.OnDateSetListener) this, year, month, day);
        //return new DatePickerDialog(getActivity(), , year, month, day);
        //return new DatePickerDialog(getActivity(), );
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        PantallaAddTarea.setDate(year, month, dayOfMonth);
    }
}
