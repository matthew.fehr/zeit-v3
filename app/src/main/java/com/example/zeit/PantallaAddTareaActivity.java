package com.example.zeit;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.zeit.Entidades.Categoria;

import java.util.ArrayList;

public class PantallaAddTareaActivity extends Fragment {

    Spinner spnCategorias;
    ArrayList<Categoria> categorias;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_add_tarea, container, false);

        populateSpinner();

        return view;
    }


    public void populateSpinner(){

        spnCategorias = getView().findViewById(R.id.spnCategoria);

        categorias = Sistema.categorias;

        ArrayList<String> nombreDeCategorias = new ArrayList<String>();

        for (int contador = 0 ; contador < categorias.size() ; contador++) {
            nombreDeCategorias.add(categorias.get(contador).getNombreCategoria());
            //System.out.println(nombreDeCategorias.get(contador));
        }

        if (nombreDeCategorias.size()==0){
            nombreDeCategorias.add("N/A");
        }

        ArrayAdapter adapter= new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, nombreDeCategorias);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Spinner spinner = (Spinner) findViewById(R.id.spnCategoria);

        spnCategorias.setAdapter(adapter);

    }




}
