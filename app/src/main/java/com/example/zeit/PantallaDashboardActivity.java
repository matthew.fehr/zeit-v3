package com.example.zeit;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.example.zeit.Entidades.Categoria;
import com.example.zeit.Entidades.Tarea;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.IllegalFormatCodePointException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class PantallaDashboardActivity extends Fragment {

    View view;
    protected FragmentActivity mActivity;

    Timer t = new Timer();

    TextView txtMensajeTareaActiva;
    TextView txtEstadoTareaActiva;
    RelativeLayout pnlTareaActiva;
    TextView txtNombreTareaActiva;
    TextView txtCategoriaTareaActiva;
    TextView txtDuracionTareaActiva;
    ImageView imgIconoPlayPause;
    ImageView imgStop;

    boolean hayTareaActiva;
    boolean tareaActivaCorriendo;

    public static boolean dashboardActivo = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.activity_dashboard, container, false);

        encontrarLasVistas();
        buscarTareaActiva();
        agregarListeners();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            mActivity =(FragmentActivity) context;
        }
    }

    private void encontrarLasVistas() {

        txtMensajeTareaActiva = view.findViewById(R.id.lblCantTareasActivas);
        txtEstadoTareaActiva = view.findViewById(R.id.lblEstado);
        pnlTareaActiva = view.findViewById(R.id.pnlTareaActiva);
        txtNombreTareaActiva = view.findViewById(R.id.nombreTarea);
        txtCategoriaTareaActiva = view.findViewById(R.id.nombreCategoria);
        txtDuracionTareaActiva = view.findViewById(R.id.duracionTarea);
        imgIconoPlayPause = view.findViewById(R.id.btnPlayPause);
        imgStop = view.findViewById(R.id.btnStop);

    }

    public void agregarListeners(){
        imgIconoPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String estado;
                if (tareaActivaCorriendo){
                    estado = "(En Pausa)";
                    imgIconoPlayPause.setImageResource(R.drawable.play_button);
                    tareaActivaCorriendo = false;
                } else {
                    estado = "(Corriendo)";
                    imgIconoPlayPause.setImageResource(R.drawable.pause2);
                    tareaActivaCorriendo = true;
                }
                txtEstadoTareaActiva.setText(estado);

                Calendar c = Calendar.getInstance();
                int yearActual = c.get(Calendar.YEAR);
                int monthActual = c.get(Calendar.MONTH);
                int dayActual = c.get(Calendar.DAY_OF_MONTH);
                int horaActual = c.get(Calendar.HOUR);
                int minutoActual = c.get(Calendar.MINUTE);

                LocalDateTime instanciaDeTiempo = LocalDateTime.of(yearActual, monthActual, dayActual, horaActual, minutoActual);

                Sistema.instanciasDeRealizacion.add(instanciaDeTiempo);
            }
        });

        imgStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                t.cancel();

                LocalDateTime instanciaDeTiempo;

                if (tareaActivaCorriendo){
                    Calendar c = Calendar.getInstance();
                    int yearActual = c.get(Calendar.YEAR);
                    int monthActual = c.get(Calendar.MONTH);
                    int dayActual = c.get(Calendar.DAY_OF_MONTH);
                    int horaActual = c.get(Calendar.HOUR);
                    int minutoActual = c.get(Calendar.MINUTE);

                    instanciaDeTiempo = LocalDateTime.of(yearActual, monthActual, dayActual, horaActual, minutoActual);

                    Sistema.instanciasDeRealizacion.add(instanciaDeTiempo);


                } else {
                    instanciaDeTiempo = Sistema.instanciasDeRealizacion.get(Sistema.instanciasDeRealizacion.size()-1);
                }

                Tarea tareaActiva = null;

                for (int contador = 0 ; contador<Sistema.tareas.size() ; contador++){
                    if (Sistema.tareas.get(contador).getIdTarea()==Sistema.idTareaActiva){
                        tareaActiva = Sistema.tareas.get(contador);
                    }
                }

                tareaActiva.setMomentoFin(instanciaDeTiempo);

                ArrayList<Long> minutosParciales = new ArrayList<>();
                //int cantInstanciasCompletas = (Sistema.instanciasDeRealizacion.size()-(Sistema.instanciasDeRealizacion.size()%2))/2;
                int cantInstanciasCompletas = Sistema.instanciasDeRealizacion.size()/2;

                int indiceInstanciaDeInicio = 0;
                for (int contador = 0 ; contador<cantInstanciasCompletas ; contador++){
                    minutosParciales.add(Duration.between(Sistema.instanciasDeRealizacion.get(indiceInstanciaDeInicio), Sistema.instanciasDeRealizacion.get(indiceInstanciaDeInicio+1)).toMinutes());
                    indiceInstanciaDeInicio = indiceInstanciaDeInicio+2;
                }

                long duracionMinutos = 0;

                for (int contador = 0 ; contador<minutosParciales.size() ; contador++){
                    duracionMinutos = duracionMinutos + minutosParciales.get(contador);
                }

                tareaActiva.setDuracionMinutosParciales(duracionMinutos);
                tareaActiva.setEstaTerminada(true);

                Sistema.idTareaActiva = -1;
                Sistema.instanciasDeRealizacion.clear();

                hayTareaActiva = false;
                tareaActivaCorriendo = false;

                dashboardActivo = false;

                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmento_contenedor, new PantallaDashboardActivity()).commit();

            }
        });

    }

    private void buscarTareaActiva() {
        if (Sistema.idTareaActiva!=-1){

            hayTareaActiva = true;
            dashboardActivo = true;

            txtMensajeTareaActiva.setText("Su tarea activa");

            Tarea tareaActiva = null;

            for (int contador = 0 ; contador<Sistema.tareas.size() ; contador++){
                if (Sistema.tareas.get(contador).getIdTarea()==Sistema.idTareaActiva){
                    tareaActiva = Sistema.tareas.get(contador);
                }
            }

            String estado;
            if (Sistema.instanciasDeRealizacion.size()%2==1){
                estado = "(Corriendo)";
                tareaActivaCorriendo = true;
                imgIconoPlayPause.setImageResource(R.drawable.pause2);
            } else {
                estado = "(En pausa)";
                tareaActivaCorriendo = false;
                imgIconoPlayPause.setImageResource(R.drawable.play_button);
            }
            txtEstadoTareaActiva.setText(estado);

            pnlTareaActiva.setVisibility(View.VISIBLE);
            txtNombreTareaActiva.setText(tareaActiva.getNombreTarea());

            Categoria categoriaTareaActiva = null;

            for (int contador = 0 ; contador<Sistema.categorias.size() ; contador++){
                if (Sistema.categorias.get(contador).getIdCategoria()==tareaActiva.getId_categoria()){
                    categoriaTareaActiva = Sistema.categorias.get(contador);
                }
            }

            txtCategoriaTareaActiva.setText(categoriaTareaActiva.getNombreCategoria());
            getDuracionTareaActiva();


        } else {
            txtEstadoTareaActiva.setText("");
            hayTareaActiva = false;
        }
    }

    private void getDuracionTareaActiva(){

        t.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (hayTareaActiva){
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (tareaActivaCorriendo){
                                String duracion = "";
                                String horas;
                                String minutos;

                                ArrayList<Long> minutosParciales = new ArrayList<>();
                                int cantInstanciasCompletas = (Sistema.instanciasDeRealizacion.size()-(Sistema.instanciasDeRealizacion.size()%2))/2;

                                Calendar c = Calendar.getInstance();
                                int yearActual = c.get(Calendar.YEAR);
                                int monthActual = c.get(Calendar.MONTH);
                                int dayActual = c.get(Calendar.DAY_OF_MONTH);
                                int horaActual = c.get(Calendar.HOUR);
                                int minutoActual = c.get(Calendar.MINUTE);

                                LocalDateTime momentoActual = LocalDateTime.of(yearActual, monthActual, dayActual, horaActual, minutoActual);

                                int indiceInstanciaDeInicio = 0;
                                for (int contador = 0 ; contador<cantInstanciasCompletas ; contador++){
                                    minutosParciales.add(Duration.between(Sistema.instanciasDeRealizacion.get(indiceInstanciaDeInicio), Sistema.instanciasDeRealizacion.get(indiceInstanciaDeInicio+1)).toMinutes());
                                    indiceInstanciaDeInicio = indiceInstanciaDeInicio+2;
                                }

                                if (tareaActivaCorriendo){
                                    minutosParciales.add(Duration.between(Sistema.instanciasDeRealizacion.get(Sistema.instanciasDeRealizacion.size()-1), momentoActual).toMinutes());
                                }

                                long duracionMinutos = 0;

                                for (int contador = 0 ; contador<minutosParciales.size() ; contador++){
                                    duracionMinutos = duracionMinutos + minutosParciales.get(contador);
                                }



                                horas = String.valueOf((duracionMinutos-duracionMinutos%60)/60);
                                minutos = String.valueOf(duracionMinutos%60);

                                duracion = horas + " h ";

                                if (duracionMinutos%60 < 10){
                                    duracion = duracion + " ";
                                }

                                duracion = duracion + minutos + " m ";

                                txtDuracionTareaActiva.setText(duracion);
                            }
                        }
                    });
                }
                }
                },
                0,5000);
    }

}
