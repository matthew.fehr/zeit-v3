package com.example.zeit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class PantallaDeCargaActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_carga);
        timerPantallaDeCarga();
    }

    private void timerPantallaDeCarga(){

        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        lanzarVistaDashboard();
                    }
                },
                2000
        );
    }

    public void lanzarVistaDashboard(){
        Intent i = new Intent(this, PantallaDashboardActivity.class);
        //i.putExtra("sistema", (Parcelable) sistema);
        startActivity(i);
    }

}
