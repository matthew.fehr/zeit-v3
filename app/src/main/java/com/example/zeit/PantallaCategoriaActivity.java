package com.example.zeit;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.zeit.Entidades.Categoria;

import java.util.ArrayList;

import afu.org.checkerframework.checker.igj.qual.I;
import afu.org.checkerframework.checker.units.qual.A;
import afu.org.checkerframework.checker.units.qual.C;

public class PantallaCategoriaActivity extends Fragment {

    public ArrayList<Categoria> categoriaArrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_categoria, container, false);

        cargarCategorias();
        ListAdapter listAdapter = new ListAdapter(view.getContext(), categoriaArrayList);
        if (!categoriaArrayList.isEmpty()) {
            ListView lista = (ListView) view.findViewById(R.id.listaCategorias);
            lista.setAdapter(listAdapter);
            onStart();
        }
        return view;
    }

    public void cargarCategorias(){
        for (int i = 0; i<Sistema.categorias.size();i++){
            if (Sistema.categorias.get(i).activo) {
                Categoria categoria = new Categoria(Sistema.categorias.get(i).idCategoria, Sistema.categorias.get(i).nombreCategoria, Sistema.categorias.get(i).limiteMinutosDiarios);
                categoriaArrayList.add(categoria);
            }
        }
    }
}
