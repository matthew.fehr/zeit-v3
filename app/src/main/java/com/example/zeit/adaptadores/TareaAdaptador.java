package com.example.zeit.adaptadores;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.zeit.Entidades.Categoria;
import com.example.zeit.Entidades.Tarea;
import com.example.zeit.R;
import com.example.zeit.Sistema;

import java.util.ArrayList;

public class TareaAdaptador extends BaseAdapter {

    private final Fragment fragmento;
    //private final ArrayList<String> nombreTareas;
    private final ArrayList<Tarea> tareas;
    private ArrayList<Categoria> categorias;

    public TareaAdaptador(Fragment fragmento, ArrayList<Tarea> tareas){
        super();
        this.fragmento = fragmento;
        //this.nombreTareas = nombreTareas;
        this.tareas = tareas;
        this.categorias = Sistema.categorias;
    }

    @Override
    public View getView(int posicion, View vista, ViewGroup padre) {
        //ArrayList<Categoria> categorias = Sistema.categorias;
        LayoutInflater inflater = fragmento.getLayoutInflater();
        View view = inflater.inflate(R.layout.elemento_lista_tareas, null, true);

        TextView nombreElemento = (TextView) view.findViewById(R.id.nombreTarea);
        nombreElemento.setText(tareas.get(posicion).getNombreTarea());

        TextView nombreCategoria = (TextView) view.findViewById(R.id.nombreCategoria);
        nombreCategoria.setText(findCategoria(posicion));

        TextView duracionTarea = (TextView) view.findViewById(R.id.duracionTarea);
        duracionTarea.setText(decirDuracion(posicion));

        return view;
    }

    private String decirDuracion(int posicion){
        String duracion = "";
        String horas;
        String minutos;

        long duracionMinutos = tareas.get(posicion).getDuracionMinutos();

        horas = String.valueOf((duracionMinutos-duracionMinutos%60)/60);
        minutos = String.valueOf(duracionMinutos%60);

        duracion = horas + " h ";

        if (duracionMinutos%60 < 10){
            duracion = duracion + " ";
        }

        duracion = duracion + minutos + " m ";

        return duracion;
    }

    private String findCategoria(int posicion){
        String nombreDeCategoria = "N/A";

        for (int contador = 0; contador<categorias.size() ; contador++){
            if (categorias.get(contador).getIdCategoria()==tareas.get(posicion).getId_categoria()){
                nombreDeCategoria = categorias.get(contador).getNombreCategoria();
            }
        }

        return nombreDeCategoria;
    }

    @Override
    public int getCount() {
        return tareas.size();
    }

    @Override
    public Object getItem(int arg0) {
        if(tareas != null){
            return tareas.get(arg0);
        }
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        //String unNombre = nombreTareas.get(arg0);
        Tarea tarea = tareas.get(arg0);
        return tarea.getIdTarea();

    }

}
