package com.example.zeit;

import android.os.Bundle;
import android.view.*;
import android.widget.LinearLayout;
import android.widget.Switch;

import androidx.annotation.*;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import com.example.zeit.Entidades.Tarea;
import com.example.zeit.adaptadores.TareaAdaptador;

import org.checkerframework.checker.units.qual.A;

import java.util.ArrayList;

public class PantallaTarea extends ListFragment {

    ArrayList<String> nombreTareas = new ArrayList<>();
    ArrayList<Tarea> tareas = new ArrayList<>();

    View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        for (int contador = 0 ; contador<Sistema.tareas.size() ; contador++){
            if (Sistema.tareas.get(contador).isEstaTerminada()){
                tareas.add(Sistema.tareas.get(contador));
            }
        }

        setListAdapter(new TareaAdaptador(this, tareas));
        view = inflater.inflate(R.layout.list_view_tareas, container, false);

        return view;
    }



}
