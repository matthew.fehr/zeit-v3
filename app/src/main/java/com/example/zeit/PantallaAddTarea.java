package com.example.zeit;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.zeit.Entidades.Categoria;
import com.example.zeit.Entidades.Tarea;
import com.example.zeit.utils.DatePickerFragment;
import com.example.zeit.utils.TimePickerFragment;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;

public class PantallaAddTarea extends Fragment{

    ArrayList<Categoria> categorias;
    View view;
    TimePicker timeInicio;

    SwitchCompat switchTareaHecha;
    SwitchCompat switchMismoDia;

    LinearLayout pnlSwitchMismoDia;
    LinearLayout pnlMomentoInicio;
    LinearLayout pnlMomentoFin;
    LinearLayout pnlMomentos;

    static Button txtHoraInicio;
    static Button txtHoraFin;
    static Button txtFechaFinTarea;
    static Button txtFechaInicioTarea;

    EditText txtNombreTarea;
    Spinner spnCategorias;

    Button btnCrearTarea;

    public static int horaInicio = -1;
    public static int minutoInicio = -1;
    public static int horaFin = -1;
    public static int minutoFin = -1;

    public static int yearInicio = -1;
    public static int monthInicio = -1;
    public static int dayInicio = -1;
    public static int yearFin = -1;
    public static int monthFin = -1;
    public static int dayFin = -1;

    public static boolean esElInicioHora;
    public static boolean esElInicioFecha;

    static String horaSeleccionada = "";
    static String fechaSeleccionada = "";

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_add_tarea, container, false);

        encontrarLasVistas();
        configurarComponentes();
        setListeners();

        return view;
    }

    public static void setTime(int hourOfDay, int minute){

        TextView txtHora;

        if (esElInicioHora){
            txtHora = txtHoraInicio;
        } else {
            txtHora = txtHoraFin;
        }

        if (hourOfDay<10){
            horaSeleccionada = "0";
        }

        horaSeleccionada = horaSeleccionada + String.valueOf(hourOfDay);
        horaSeleccionada = horaSeleccionada + " : ";

        if (minute<10){
            horaSeleccionada = horaSeleccionada + "0";
        }

        horaSeleccionada = horaSeleccionada + String.valueOf(minute);

        txtHora.setText(horaSeleccionada);
        txtHora.setTextColor(Color.WHITE);

        if (esElInicioHora){
            horaInicio = hourOfDay;
            minutoInicio = minute;
        } else {
            horaFin = hourOfDay;
            minutoFin = minute;
        }

        horaSeleccionada = "";
    }

    public static void setDate(int year, int month, int dayOfMonth){

        Button txtFecha;

        if (esElInicioFecha){
            txtFecha = txtFechaInicioTarea;
        } else {
            txtFecha = txtFechaFinTarea;
        }



        fechaSeleccionada = fechaSeleccionada + String.valueOf(dayOfMonth);
        fechaSeleccionada = fechaSeleccionada + " / ";
        fechaSeleccionada = fechaSeleccionada + String.valueOf(month+1);
        fechaSeleccionada = fechaSeleccionada + " / ";
        fechaSeleccionada = fechaSeleccionada + String.valueOf(year);

        txtFecha.setText(fechaSeleccionada);
        txtFecha.setTextColor(Color.WHITE);

        if (esElInicioFecha){
            yearInicio = year;
            monthInicio = month;
            dayInicio = dayOfMonth;
        } else {
            yearFin = year;
            monthFin = month;
            dayFin = dayOfMonth;
        }

        fechaSeleccionada = "";
    }

    private void configurarComponentes(){
        populateSpinner();
    }

    public void populateSpinner(){

        categorias = Sistema.categorias;

        ArrayList<String> nombreDeCategorias = new ArrayList<>();

        for (int contador = 0 ; contador < categorias.size() ; contador++) {
            nombreDeCategorias.add(categorias.get(contador).getNombreCategoria());
            //System.out.println(nombreDeCategorias.get(contador));
        }

        if (nombreDeCategorias.size()==0){
            nombreDeCategorias.add("N/A");
        }

        ArrayAdapter adapter= new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, nombreDeCategorias);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Spinner spinner = (Spinner) findViewById(R.id.spnCategoria);

        spnCategorias.setAdapter(adapter);

    }

    private void encontrarLasVistas(){
        switchTareaHecha = view.findViewById(R.id.switchModo);
        pnlSwitchMismoDia = view.findViewById(R.id.pnlSwitchMismoDia);
        switchMismoDia = view.findViewById(R.id.switchMismoDia);
        pnlMomentoInicio = view.findViewById(R.id.pnlMomentoInicio);
        pnlMomentoFin = view.findViewById(R.id.pnlMomentoFin);
        pnlMomentos = view.findViewById(R.id.pnlMomentos);
        btnCrearTarea = view.findViewById(R.id.btnCrearTarea);
        txtNombreTarea = view.findViewById(R.id.txtNombreDeTarea);
        spnCategorias = view.findViewById(R.id.spnCategoria);

        txtFechaFinTarea = view.findViewById(R.id.txtFechaFinTarea);
        txtFechaInicioTarea = view.findViewById(R.id.txtFechaInicioTarea);
        txtHoraInicio = view.findViewById(R.id.txtHoraInicioTarea);
        txtHoraFin = view.findViewById(R.id.txtHoraFinTarea);

    }

    private void setListeners(){

        switchTareaHecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (switchTareaHecha.isChecked()){
                    switchTareaHecha.setText(view.getResources().getText(R.string.switchModoNoRealizada));
                    pnlMomentoFin.setVisibility(View.GONE);
                    pnlSwitchMismoDia.setVisibility(View.GONE);
                    pnlMomentos.setWeightSum(1);

                    txtHoraInicio.setEnabled(false);
                    txtFechaInicioTarea.setEnabled(false);

                    Calendar c = Calendar.getInstance();
                    yearInicio = c.get(Calendar.YEAR);
                    monthInicio = c.get(Calendar.MONTH);
                    dayInicio = c.get(Calendar.DAY_OF_MONTH);

                    horaInicio = c.get(Calendar.HOUR);
                    minutoInicio = c.get(Calendar.MINUTE);

                    esElInicioHora = true;
                    esElInicioFecha = true;

                    setTime(horaInicio, minutoInicio);
                    setDate(yearInicio, monthInicio, dayInicio);

                    btnCrearTarea.setText("INICIAR TAREA");

                } else {
                    switchTareaHecha.setText(view.getResources().getText(R.string.switchModoRealizada));
                    pnlMomentos.setWeightSum(2);
                    pnlMomentoFin.setVisibility(View.VISIBLE);
                    pnlSwitchMismoDia.setVisibility(View.VISIBLE);

                    txtHoraInicio.setEnabled(true);
                    txtFechaInicioTarea.setEnabled(true);
                    txtHoraInicio.setText("");
                    txtFechaInicioTarea.setText("");

                    txtFechaFinTarea.setText("");
                    txtHoraFin.setText("");

                    horaInicio = -1;
                    minutoInicio = -1;
                    horaFin = -1;
                    minutoFin = -1;
                    yearInicio = -1;
                    monthInicio = -1;
                    dayInicio = -1;
                    yearFin = -1;
                    monthFin = -1;
                    dayFin = -1;

                    btnCrearTarea.setText("AGREGAR TAREA");
                }

            }
        });

        switchMismoDia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (switchMismoDia.isChecked()){
                    txtFechaFinTarea.setVisibility(View.GONE);
                    switchMismoDia.setText(view.getResources().getText(R.string.switchMismoDiaTrue));
                } else {
                    txtFechaFinTarea.setVisibility(View.VISIBLE);
                    switchMismoDia.setText(view.getResources().getText(R.string.switchMismoDiaFalse));
                }

            }
        });

        btnCrearTarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                guardarTarea();

            }
        });

        txtFechaInicioTarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarDatePickerInicio(v);
            }
        });

        txtFechaFinTarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarDatePickerFin(v);
            }
        });

        txtHoraInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarTimePickerInicio(v);
            }
        });

        txtHoraFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarTimePickerFin(v);
            }
        });
    }

    public void guardarTarea(){

        if (!txtNombreTarea.getText().toString().isEmpty()){

            int indiceCategoria = spnCategorias.getSelectedItemPosition();
            Categoria categoriaDeLaTarea = categorias.get(indiceCategoria);

            String nombreTarea = txtNombreTarea.getText().toString();
            int idCategoriaDeLaTarea = categoriaDeLaTarea.getIdCategoria();

            if (yearInicio!=-1){

                if ((!switchTareaHecha.isChecked() && yearFin!=-1) || switchTareaHecha.isChecked() || switchMismoDia.isChecked()){

                    if ((horaInicio!=-1 && horaFin!=-1) || (horaInicio!=-1 && switchTareaHecha.isChecked())){

                        if (switchMismoDia.isChecked()){
                            yearFin = yearInicio;
                            monthFin = monthInicio;
                            dayFin = dayInicio;
                        }

                        String ldtInicio = conseguirHoraInicio();
                        String ldtFin = conseguirHoraFin();

                        LocalDateTime momentoInicio;
                        LocalDateTime momentoFin;

                        boolean estaTerminada = true;

                        if (switchTareaHecha.isChecked()){
                            momentoFin = null;
                            estaTerminada = false;
                            Calendar c = Calendar.getInstance();
                            momentoInicio = LocalDateTime.of(yearInicio, monthInicio, dayInicio, horaInicio, minutoInicio);
                        } else {
                            momentoInicio = LocalDateTime.parse(ldtInicio, formatter);
                            momentoFin = LocalDateTime.parse(ldtFin, formatter);
                        }

                        Tarea tarea = new Tarea(
                                Sistema.tareas.size()+1,
                                nombreTarea,
                                momentoInicio,
                                momentoFin,
                                0,
                                idCategoriaDeLaTarea,
                                estaTerminada
                        );

                        if (tarea.isEstaTerminada()){
                            tarea.setDuracionMinutos();
                        }

                        if (tarea.getDuracionMinutos() >= 0){
                            if (switchTareaHecha.isChecked() && Sistema.idTareaActiva==-1 || !switchTareaHecha.isChecked()){
                                Sistema.tareas.add(tarea);

                                if (switchTareaHecha.isChecked()){
                                    Sistema.idTareaActiva = tarea.getIdTarea();
                                    Sistema.instanciasDeRealizacion.add(momentoInicio);
                                }

                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmento_contenedor, new PantallaTarea()).commit();

                                horaInicio = -1;
                                minutoInicio = -1;
                                horaFin = -1;
                                minutoFin = -1;

                                yearInicio = -1;
                                monthInicio = -1;
                                dayInicio = -1;
                                yearFin = -1;
                                monthFin = -1;
                                dayFin = -1;
                            } else {
                                Toast.makeText(getActivity(), "Ya existe una tarea activa en este momento",
                                        Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(getActivity(), "El momento de finalizacion no puede ser previo al de inicio",
                                    Toast.LENGTH_SHORT).show();
                        }

                    } else {

                        if (switchTareaHecha.isChecked()){
                            Toast.makeText(getActivity(), "Debe seleccionar una hora de inicio",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "Debe seleccionar una hora de inicio y finalizacion",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                } else {
                    if (switchMismoDia.isChecked()){
                        Toast.makeText(getActivity(), "Debe seleccionar una hora de finalizacion",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "Debe seleccionar una fecha de finalizacion",
                                Toast.LENGTH_SHORT).show();
                    }
                }

            } else {
                if (switchMismoDia.isChecked() || switchTareaHecha.isChecked()){
                    Toast.makeText(getActivity(), "Debe seleccionar una fecha de inicio",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Debe seleccionar una fecha de inicio y finalizacion",
                            Toast.LENGTH_SHORT).show();
                }
            }

        } else {
            Toast.makeText(getActivity(), "Debe dar un nombre a su tarea",
                    Toast.LENGTH_SHORT).show();
        }

    }

    private String conseguirHoraInicio(){
        String ldtInicio = yearInicio+"-";
        if (monthInicio<10){
            ldtInicio = ldtInicio+"0"+monthInicio;
        } else {
            ldtInicio = ldtInicio + monthInicio;
        }
        ldtInicio = ldtInicio + "-";
        if (dayInicio<10){
            ldtInicio = ldtInicio+"0"+dayInicio;
        } else {
            ldtInicio = ldtInicio + dayInicio;
        }
        ldtInicio = ldtInicio + " ";

        if (horaInicio<10){
            ldtInicio = ldtInicio + "0" + horaInicio;
        } else {
            ldtInicio = ldtInicio + horaInicio;
        }
        ldtInicio = ldtInicio + ":";
        if (minutoInicio<10){
            ldtInicio = ldtInicio + "0" + minutoInicio;
        } else {
            ldtInicio = ldtInicio + minutoInicio;
        }

        return ldtInicio;
    }

    private String conseguirHoraFin(){
        String ldtFin = yearFin+"-";
        if (monthFin<10){
            ldtFin = ldtFin+"0"+monthFin;
        } else {
            ldtFin = ldtFin + monthFin;
        }
        ldtFin = ldtFin + "-";
        if (dayFin<10){
            ldtFin = ldtFin+"0"+dayFin;
        } else {
            ldtFin = ldtFin + dayFin;
        }
        ldtFin = ldtFin + " ";

        if (horaFin<10){
            ldtFin = ldtFin + "0" + horaFin;
        } else {
            ldtFin = ldtFin + horaFin;
        }
        ldtFin = ldtFin + ":";
        if (minutoFin<10){
            ldtFin = ldtFin + "0" + minutoFin;
        } else {
            ldtFin = ldtFin + minutoFin;
        }

        return ldtFin;
    }

    public void mostrarTimePickerInicio(View view){
        esElInicioHora = true;
        DialogFragment timePicker = new TimePickerFragment();
        timePicker.show(getActivity().getSupportFragmentManager(), "Seleccione la hora");
    }

    public void mostrarTimePickerFin(View view){
        esElInicioHora = false;
        DialogFragment timePicker = new TimePickerFragment();
        timePicker.show(getActivity().getSupportFragmentManager(), "Seleccione la hora");
    }

    public void mostrarDatePickerInicio(View view){
        esElInicioFecha = true;
        DialogFragment datePicker = new DatePickerFragment();
        datePicker.show(getActivity().getSupportFragmentManager(), "Seleccione la fecha");
    }

    public void mostrarDatePickerFin(View view){
        esElInicioFecha = false;
        DialogFragment datePicker = new DatePickerFragment();
        datePicker.show(getActivity().getSupportFragmentManager(), "Seleccione la fecha");
    }
}
