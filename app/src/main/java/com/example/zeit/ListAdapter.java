package com.example.zeit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.zeit.Entidades.Categoria;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ListAdapter extends ArrayAdapter<Categoria> {

    public ListAdapter(Context context, ArrayList<Categoria> categoriaArrayList){
        super(context,R.layout.elemento_list_categoria,categoriaArrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Categoria categoria = getItem(position);
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.elemento_list_categoria,parent,false);
        }
        ImageButton btnEliminar = (ImageButton) convertView.findViewById(R.id.iconDelete);
        ImageButton btnEditar = (ImageButton) convertView.findViewById(R.id.iconEdit);
        TextView nombreCategoria = convertView.findViewById(R.id.lblCategoria);
        TextView minutosDisponible = convertView.findViewById(R.id.intMinutosDisponible);

        nombreCategoria.setText(categoria.nombreCategoria);
        minutosDisponible.setText(String.valueOf(categoria.limiteMinutosDiarios));


        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i<Sistema.categorias.size(); i++){
                    if (Sistema.categorias.get(i).idCategoria==categoria.idCategoria){
                        Sistema.categorias.get(i).setActivo(false);
                    }
                }
            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i<Sistema.categorias.size(); i++){
                    if (Sistema.categorias.get(i).idCategoria==categoria.idCategoria){

                    }
                }
            }
        });


        return convertView;
    }
}