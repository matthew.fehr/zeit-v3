package com.example.zeit;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.zeit.Entidades.Categoria;
import com.example.zeit.Entidades.Tarea;
import com.example.zeit.utils.DatePickerFragment;
import com.example.zeit.utils.TimePickerFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Sistema extends AppCompatActivity{

    private long ms = 0;
    private long splashTime = 2000;
    private boolean splashActive = true;
    private boolean paused = false;

    public static int idTareaActiva = -1;

    public Categoria categoria;

    public static boolean formato24Horas = false;

    PantallaCategoriaActivity pnlAddCategoria =new PantallaCategoriaActivity();

    public static ArrayList<Categoria> categorias =new ArrayList<Categoria>();
    public static ArrayList<Tarea> tareas =new ArrayList<Tarea>();
    public static ArrayList<LocalDateTime> instanciasDeRealizacion = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_carga);

        Thread mythread = new Thread() {
            @Override
            public void run() {
                try {
                    while (splashActive && ms < splashTime) {
                        if(!paused)
                            ms=ms+100;
                        sleep(100);
                    }
                } catch(Exception e) {
                    System.out.println("Error al correr el timer: "+e.getMessage());
                }
                finally {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setContentView(R.layout.activity_sistema);

                            BottomNavigationView barraNav = findViewById(R.id.bottom_navigation);
                            barraNav.setOnNavigationItemSelectedListener(listenerBarra);
                            barraNav.setSelectedItemId(R.id.nav_dashboard);

                            getSupportFragmentManager().beginTransaction().replace(R.id.fragmento_contenedor,
                                    new com.example.zeit.PantallaDashboardActivity()).commit();

                            /*db = Room.databaseBuilder(getApplicationContext(),
                                    ZeitDatabase.class, "ZeitDatabase").build();*/

                            cargarCategoriaDePrueba();
                            cargarTareasDePrueba();
                        }
                    });
                }
            }
        };
        mythread.start();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener listenerBarra =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment fragmentoSeleccionado = null;

                    PantallaDashboardActivity.dashboardActivo = false;

                    switch(item.getItemId()){
                        case R.id.nav_categoria:
                            fragmentoSeleccionado = new PantallaCategoriaActivity();
                            break;
                        case R.id.nav_dashboard:
                            fragmentoSeleccionado = new com.example.zeit.PantallaDashboardActivity();
                            break;
                        case R.id.nav_add_tarea:
                            fragmentoSeleccionado = new com.example.zeit.PantallaTarea();
                            break;
                        case R.id.nav_configuracion:
                            fragmentoSeleccionado = new com.example.zeit.PantallaConfiguracionActivity();
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragmento_contenedor,
                            fragmentoSeleccionado).commit();

                    return true;
                }
            };

    public void lanzarVistaCategoria(View view){
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmento_contenedor,
                new PantallaCategoriaActivity()).commit();
    }

    public void lanzarVistaAddCategoria(View view){
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmento_contenedor,
                new PantallaCrearCategoriaActivity()).commit();
    }

    public void lanzarVistaAddTarea(View view){
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmento_contenedor,
                new com.example.zeit.PantallaAddTarea()).commit();
    }

    public void guardarCategoria(View view){
        EditText txtNombreCategoria = findViewById(R.id.txtNombreDeCategoria);
        EditText txtLimiteMinutos = findViewById(R.id.txtTiempoLimiteDiario);
        int idCategoria = categorias.size()+1;
        categoria = new Categoria(idCategoria, txtNombreCategoria.getText().toString(), Integer.parseInt(txtLimiteMinutos.getText().toString()));
        categorias.add(categoria);

        lanzarVistaCategoria(view);

        //pnlAddCategoria.refrescar();
    }

    private void cargarCategoriaDePrueba(){
        Categoria cat1 = new Categoria(0, "Deporte", 120);
        Categoria cat2 = new Categoria(1, "Entretenimiento", 180);
        Categoria cat3 = new Categoria(2, "Hogar", 360);

        categorias.add(cat1);
        categorias.add(cat2);
        categorias.add(cat3);
    }

    private void cargarTareasDePrueba(){
        Tarea tarea1 = new Tarea();
        Tarea tarea2 = new Tarea();
        Tarea tarea3 = new Tarea();

        tarea1.setNombreTarea("Sacar a pasear a cochi");
        tarea2.setNombreTarea("Ir al super");
        tarea3.setNombreTarea("Desarrollar la app");

        tarea1.setId_categoria(0);
        tarea2.setId_categoria(1);
        tarea3.setId_categoria(2);

        tareas.add(tarea1);
        tareas.add(tarea2);
        tareas.add(tarea3);
    }
}
