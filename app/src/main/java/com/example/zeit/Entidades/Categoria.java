package com.example.zeit.Entidades;

import com.example.zeit.Sistema;

import java.util.ArrayList;

/*@Entity*/
public class Categoria {

    public int idCategoria;
    public String nombreCategoria;
    public int limiteMinutosDiarios;
    public boolean activo = true;

    public Categoria() {
    }

    public Categoria(int idCategoria, String nombreCategoria, int limiteMinutosDiarios) {
        this.idCategoria = idCategoria;
        this.nombreCategoria = nombreCategoria;
        this.limiteMinutosDiarios = limiteMinutosDiarios;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public int getLimiteMinutosDiarios() {
        return limiteMinutosDiarios;
    }

    public void setLimiteMinutosDiarios(int limiteMinutosDiarios) {
        this.limiteMinutosDiarios = limiteMinutosDiarios;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

}
