package com.example.zeit.Entidades;

import android.os.Build;
import android.provider.SyncStateContract;

import androidx.annotation.RequiresApi;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.example.zeit.Entidades.Categoria;
import com.example.zeit.utils.LocalDateTimeConverter;

import org.apache.commons.codec.digest.UnixCrypt;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.Calendar;

public class Tarea {

    public int idTarea;
    public String nombreTarea;
    public LocalDateTime momentoInicio;
    public LocalDateTime momentoFin;
    public long duracionMinutos;
    public int id_categoria;
    public boolean estaTerminada;

    public Tarea() {
    }

    public Tarea(int idTarea, String nombreTarea, LocalDateTime momentoInicio, LocalDateTime momentoFin, long duracionMinutos, int id_categoria, boolean estaTerminada) {
        this.idTarea = idTarea;
        this.nombreTarea = nombreTarea;
        this.momentoInicio = momentoInicio;
        this.momentoFin = momentoFin;
        this.duracionMinutos = duracionMinutos;
        this.id_categoria = id_categoria;
        this.estaTerminada = estaTerminada;
    }

    public int getIdTarea() {
        return idTarea;
    }

    public void setIdTarea(int idTarea) {
        this.idTarea = idTarea;
    }

    public String getNombreTarea() {
        return nombreTarea;
    }

    public void setNombreTarea(String nombreTarea) {
        this.nombreTarea = nombreTarea;
    }

    public LocalDateTime getMomentoInicio() {
        return momentoInicio;
    }

    public void setMomentoInicio(LocalDateTime momentoInicio) {
        this.momentoInicio = momentoInicio;
    }

    public LocalDateTime getMomentoFin() {
        return momentoFin;
    }

    public void setMomentoFin(LocalDateTime momentoFin) {
        this.momentoFin = momentoFin;
    }

    public long getDuracionMinutos() {
        return duracionMinutos;
    }

    public void setDuracionMinutos() {
        Duration diferencia = Duration.between(momentoInicio, momentoFin);
        this.duracionMinutos = diferencia.toMinutes();
    }

    public void setDuracionMinutosParciales(long duracion) {
        this.duracionMinutos = duracion;
    }

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public void setDuracionMinutos(long duracionMinutos) {
        this.duracionMinutos = duracionMinutos;
    }

    public boolean isEstaTerminada() {
        return estaTerminada;
    }

    public void setEstaTerminada(boolean estaTerminada) {
        this.estaTerminada = estaTerminada;
    }
}
